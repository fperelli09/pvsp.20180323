# Player vs Player Code Dojo

Welcome to the March 23rd 2018 Code Dojo session!


## The Contest

Today we're here to solve the problem below. Rules:

* Head-to-head coding competition
* Teams of 1 to a few
* Solve the challenge within constraints
* Friendly Player vs Player

## Winner selection

A poll named "March 23rd 2018 Code Dojo" will be available in BA Code Dojo HipChat room, each option will match the team's name (e.g. __jdoe__, __spaceinvaders__)

## Getting started

The following steps are just a suggestion on how to add your code to the project:

* Clone this project: 
```
git clone git@bitbucket.org:webcomdojoteam/pvsp.20180323.git
```
* Create a folder with your name or your team's name (say __jdoe__ or __spaceinvaders__) and make that your working directory: 
```
mkdir pvsp.20180323/spaceinvaders && cd pvsp.20180323/spaceinvaders
```
* Get your magic done (i.e. create source code files, resources, tests, docs, pom/makefiles, etc.)
* Commit your code: 
```
git add * && git commit -m 'my comments here' && git push
```

## Today's problem: Largest Permutation

You are given an array of __N__ integers which is a permutation of the first __N__ natural numbers. You can swap any two elements of the array. You can make at most __K__ swaps. What is the largest permutation, in numerical order, you can make?

### Input Format 
The first line of the input contains two integers, __N__ and __K__, the size of the input array and the maximum swaps you can make, respectively. The second line of the input contains a permutation of the first __N__ natural numbers.

### Output Format 
Print the lexicographically largest permutation you can make with at most  swaps.

### Constraints 
* 1 <= N <= 10^5
* 1 <= K <= 10^9

### Sample Input#00
```
5 1
4 2 3 5 1
```

### Sample Output#00
```
5 2 3 4 1
```

### Explanation#00 
You can swap any two numbers in [4, 2, 3, 5, 1] and see the largest permutation is [5, 2, 3, 4, 1].

### Sample Input#01
```
3 1
2 1 3
```

### Sample Output#01
```
3 1 2
```

### Explanation#01 
With 1 swap we can get [1, 2, 3], [3, 1, 2] and [2, 3, 1] out of these [3, 1, 2] is the largest permutation.

### Sample Input#02
```
2 1
2 1
```

### Sample Output#02
```
2 1
```

### Explanation#02 
We can see that [2, 1] is already the largest permutation. So we don't need any swaps.

