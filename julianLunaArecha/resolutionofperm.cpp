#include <iostream>
#include <cmath>
#include <unordered_map>
#include <vector>
#include <cstdio>
#include <algorithm>

using namespace std;


//creo un mapa con todo

int main() {

	int N, K;
	cin>>N>>K;
	vector<int>arr;
	arr.resize(N);
	unordered_map<int, int>indice_de_todos;

//lo meto en un array

	for(int i=0; i<N; i++) {

		cin>>arr[i];
		indice_de_todos[arr[i]] = i;

  	}

// lo reordeno, tomo el mas grande y lo mando al frente

	for(int i=0; i<N; i++) {

		if(K==0) {

			break;

		}

		if(arr[i]==N-i) {

			continue;

		}
    
    	indice_de_todos[arr[i]]=indice_de_todos[N-i];
    	arr[indice_de_todos[N-i]]=arr[i];
    	arr[i]=N-i;
    	K--;

	}

  for(int i=0; i<N; i++) {

    cout<<arr[i]<<" ";

  }

  return 0;

}