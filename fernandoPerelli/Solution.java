import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
class Solution {

    static int[] largestPermutation(int k, int[] arr) {
    	int[] tempArr = arr;
    	int[] kGreaters =  getTheKGreaters(k, arr);
    	
    	int temp = 0;
    	for (int i = 0; i < kGreaters.length; i++) {
    		temp = arr[i];
    		tempArr[i] = tempArr[i];
    		tempArr[kGreaters[i]] = temp;
		}
    
    }

    private static int[] getTheKGreaters(int k, int[] arr) {
    	int i = 0;
    	int[] kArray= new int[k];
    	java.util.List<Integer> intList = getIntegerList(arr);
    	
    	while(i <k) {
    		kArray[i] = intList.indexOf(Collections.max(intList));
    		intList.remove(i);
    		i++;
    	}
    	
		return kArray;
	}

	private static List<Integer> getIntegerList(int[] arr) {
		List<Integer> result = new LinkedList<Integer>();
		for (int i = 0; i < arr.length; i++) {
			result.add(arr[i]);
		}
		return result;
	}

	static void Main(String[] args) {
        string[] tokens_n = Console.ReadLine().Split(' ');
        int n = Convert.ToInt32(tokens_n[0]);
        int k = Convert.ToInt32(tokens_n[1]);
        string[] arr_temp = Console.ReadLine().Split(' ');
        int[] arr = Array.ConvertAll(arr_temp,Int32.Parse);
        int[] result = largestPermutation(k, arr);
        Console.WriteLine(String.Join(" ", result));


    }
}
